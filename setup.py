#! /usr/bin/env python3

# Dependency
import setuptools
# Project
import metriculous

setuptools.setup(
    author="Ted Moseley",
    author_email="tmoseley1106@gmail.com",
    description="A Tool for Gathering Meticulous Metrics",
    long_description=open("README.rst").read(),
    url="https://gitlab.com/tmose1106/metrics",
    name="metriculous",
    version=metriculous.__version__,
    packages=["metriculous"],
    include_package_data=True,
    license="MIT License",
    entry_points={
        'console_scripts': ["metriculous = metriculous.main:main", ],
    },
    platforms='any',
    zip_safe=False,
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Testing',
        'Natural Language :: English',
    ],
)
