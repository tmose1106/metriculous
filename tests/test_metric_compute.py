# Standard
import collections
import os
# Dependency
import pygments.lexers
import pytest
# Project
from metriculous import compute, stock_metrics

METRICS = collections.OrderedDict([
    ("sloc", stock_metrics.SLOCMetric({})),
    ("comment", stock_metrics.CommentMetric({})),
    ("mccabe", stock_metrics.McCabeMetric({})),
])

PROCESSOR = compute.ResultProcessor(METRICS, {})

RESULT_PROCESSOR_PARAMS = [
    (("sample_c_files", "sample_1.c"), [13, 5, 1]),
    (("sample_cpp_files", "sample_1.cpp"), [11, 4, 1]),
    (("sample_cpp_files", "sample_2.cpp"), [22, 37, 1]),
    (("sample_java_files", "sample_1.java"), [27, 6, 1]),
    (("sample_java_files", "multiline_comment.java"), [0, 1, 0]),
    (("sample_js_files", "sample_2.js"), [1, 1, 0]),
    (("sample_python_files", "sample_1.py"), [18, 7, 0]),
    (("sample_ruby_files", "sample_1.rb"), [10, 8, 0]),
    (("sample_ruby_files", "sample_2.rb"), [34, 11, 0]),
]


@pytest.mark.parametrize("file_path_parts,expected_result", RESULT_PROCESSOR_PARAMS)
def test_result_processor(file_path_parts, expected_result):
    """"""
    file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                             *file_path_parts)
    lexer = pygments.lexers.get_lexer_for_filename(file_path)
    result = PROCESSOR.process_string(open(file_path).read(), lexer)

    assert result == expected_result
