"""
metrics - produce a variety of metrics from source code files

Copyright (c) 2005 by Reg. Charney <charney@charneyday.com>`

All rights reserved, see LICENSE for details.
"""
__version__ = "0.3.0"
__version_info__ = (0, 3, 0)
