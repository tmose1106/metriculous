*****************************************************
Metriculous - A Tool for Gathering Meticulous Metrics
*****************************************************

Metriculous is a tool for calculating the source lines of code (aka **SLOC**)
for any programming project. It is powered by Pygments_, allowing it to parse
a large number of programming languages.

Usage
=====

::

  Usage: metrics [OPTIONS] [PATHS]...

    A Tool For Calculating SLOC and More

  Options:
    --format [csv|json|table|xml]  Choose a format to output
    --type [file|language]         Choose a table type to output
    --include TEXT                 Comma separate list of metrics to run
    --verbose                      Increase verbosity of output
    --version                      Show the version and exit.
    --help                         Show this message and exit.

Features
========

This tool has the ability to produce a few different output types:

* Plain Text Table
* CSV
* JSON
* XML

Here is a sample of the table output:

::

  Metrics Summary:

  Filename                   Language  SLOC  Comments  McCabe
  -------------------------- --------- ----- --------- -------
  metrics/compute.py         Python    104   62        14
  metrics/__init__.py        Python    3     6         0
  metrics/main.py            Python    142   41        21
  metrics/metric_base.py     Python    16    19        0
  metrics/output_formats.py  Python    78    18        18
  metrics/stock_metrics.py   Python    55    19        7
  -------------------------- --------- ----- --------- -------
                                       398   165       60

Installation
============

Metrics is easily installed using Pip_ and Setuptools_. The instructions below
are fairly general towards all Python projects which use Setuptools.

User Install
------------

This installation method allows for usage by a singular user and does not
require root access.

::

  git clone https://gitlab.com/tmose1106/metrics.git
  cd metrics
  python3 -m pip install -r requirements.txt --user
  python3 setup.py install --user

System-wide
-----------

This installation method allows for usage by all users on the system, but
requires root access.

::

  git clone https://gitlab.com/tmose1106/metrics.git
  cd metrics
  python3 -m pip install -r requirements.txt
  python3 setup.py install

Testing (on Unix-like systems)
------------------------------

This installation method allows for the user to modify the source code
locally and have their changes applied instantly, which is beneficial
for testing.

::

  git clone https://gitlab.com/tmose1106/metrics.git
  cd metrics
  python3 -m venv env
  source env/bin/activate
  pip install -r requirements.txt
  python setup.py develop

History
=======

While searching for a tool to compute metrics of software projects, I happened
upon an abandoned project called Metrics_ on PyPi_. When I came across the
package, it had been without an update since August of 2013 (it happened to be
January of 2017 at the time) and was written in Python 2. Depsite this, the
project produced somewhat reliable results compared to the well-known
sloccount_ project. Interestingly, Metrics also supported heterogeneous
software projects which was what I was looking for at the time.

Since then I have ported the functionality of the program over to Python 3
and rebranded the project under the new name Metriculous. Much of the original
code has been altered to be cleaner and more DRY. The original functionality
remains, plus some new features like producing JSON-formatted output.

Licensing
=========

Metrics is released under the terms of the MIT Open Source License. Check out
the LICENSE.txt_ file for more details.

.. _LICENSE.txt: ./LICENSE.txt
.. _Metrics: https://pypi.python.org/pypi/metrics
.. _Pip: https://pip.pypa.io/en/stable/
.. _Pygments: http://pygments.org/
.. _PyPi: https://pypi.python.org/pypi
.. _Setuptools: https://setuptools.readthedocs.io/en/latest/
.. _sloccount: https://www.dwheeler.com/sloccount/
